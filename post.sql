CREATE TABLE `ashilda`.`post` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT NULL,
  `text` VARCHAR(4096) NULL,
  `date` VARCHAR(128) NULL,
  PRIMARY KEY (`id`));