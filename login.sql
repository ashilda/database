CREATE TABLE `ashilda`.`login` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT NULL,
  `token` VARCHAR(128) NULL,
  `date` VARCHAR(128) NULL,
  `expired` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));